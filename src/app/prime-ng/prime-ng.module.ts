import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {MenubarModule} from 'primeng/menubar';
import {InputTextModule} from "primeng/inputtext";
import {SidebarModule} from 'primeng/sidebar';
import {ButtonModule} from "primeng/button";
import {RippleModule} from 'primeng/ripple';
import {DataViewModule} from "primeng/dataview";
import {DropdownModule} from "primeng/dropdown";
import {RatingModule} from "primeng/rating";
import {PanelModule} from "primeng/panel";
import {DialogModule} from "primeng/dialog";
import {TableModule} from "primeng/table";
import {FieldsetModule} from "primeng/fieldset";
import {InputNumberModule} from "primeng/inputnumber";
import {InputTextareaModule} from "primeng/inputtextarea";
import {FileUploadModule} from "primeng/fileupload";


@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        MenubarModule
    ],
    exports: [
        MenubarModule,
        InputTextModule,
        SidebarModule,
        ButtonModule,
        RippleModule,
        DataViewModule,
        DropdownModule,
        RatingModule,
        PanelModule,
        DialogModule,
        TableModule,
        FieldsetModule,
        InputNumberModule,
        InputTextareaModule,
        FileUploadModule,
    ]
})
export class PrimeNgModule { }
