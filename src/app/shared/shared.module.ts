import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';
import {PrimeNgModule} from "../prime-ng/prime-ng.module";
import { SidebarComponent } from './sidebar/sidebar.component';



@NgModule({
    declarations: [
        NavbarComponent,
        SidebarComponent
    ],
    exports: [
        NavbarComponent,
        SidebarComponent
    ],
    imports: [
        CommonModule,
        PrimeNgModule,
    ]
})
export class SharedModule { }
