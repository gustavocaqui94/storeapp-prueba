import {AfterViewChecked, AfterViewInit, Component, DoCheck, Input, OnInit} from '@angular/core';
import {PrimeNGConfig} from "primeng/api";

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit, AfterViewChecked{
    visibleSidebar1!:boolean;
    @Input() visibleSidebar !:boolean;

    constructor(private primengConfig: PrimeNGConfig) {}

    ngOnInit() {
        this.primengConfig.ripple = true;
        // console.log(this.visibleSidebar)
    }
    ngAfterViewChecked(): void {
        // console.log(this.visibleSidebar)
    }
}
