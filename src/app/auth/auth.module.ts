import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthRoutingModule } from './auth-routing.module';
import { MainComponent } from './pages/main/main.component';
import {PrimeNgModule} from "../prime-ng/prime-ng.module";


@NgModule({
    declarations: [
        MainComponent
    ],
    imports: [
        CommonModule,
        AuthRoutingModule,
        PrimeNgModule
    ]
})
export class AuthModule { }
