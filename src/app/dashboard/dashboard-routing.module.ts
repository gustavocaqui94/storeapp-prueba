import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from "./pages/home/home.component";
import {ProductsComponent} from "./pages/products/products.component";
import {NewProductComponent} from "./pages/new-product/new-product.component";
import {EditProductComponent} from "./pages/edit-product/edit-product.component";

const routes: Routes = [
    {
        path: '',
        component: HomeComponent,
        children: [
            {
                path: 'products',
                component: ProductsComponent,
            },
            {
                path: 'new-product',
                component: NewProductComponent,
            },
            {
                path: 'edi-product/:id',
                component: EditProductComponent,
            },
            {
                path: '**',
                redirectTo: 'products',
            }
        ]
    },
    {
        path: '**',
        redirectTo: '',
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
