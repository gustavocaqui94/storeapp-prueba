import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { HomeComponent } from './pages/home/home.component';
import {PrimeNgModule} from "../prime-ng/prime-ng.module";
import {SharedModule} from "../shared/shared.module";
import { ProductsComponent } from './pages/products/products.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { NewProductComponent } from './pages/new-product/new-product.component';
import { EditProductComponent } from './pages/edit-product/edit-product.component';


@NgModule({
    declarations: [
        HomeComponent,
        ProductsComponent,
        NewProductComponent,
        EditProductComponent,
    ],
    imports: [
        CommonModule,
        DashboardRoutingModule,
        PrimeNgModule,
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
    ]
})
export class DashboardModule { }
