import {Component, OnInit} from '@angular/core';
import {CategoryService} from "../../../services/category.service";
import {Category, Product, ProductDTO} from "../../../interfaces/product.interface";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ProductService} from "../../../services/product.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-new-product',
  templateUrl: './new-product.component.html',
  styleUrls: ['./new-product.component.scss']
})
export class NewProductComponent implements OnInit{
    public categories: Array<Category> = [];
    public productDto!: ProductDTO ;
    public linkImg: string = `https://placeimg.com/640/480/tech/sepia`

    public Form: FormGroup = this.fb.group({
        title       : ['', [Validators.required]],
        price       : ['', [Validators.required]],
        description : ['', [Validators.required]],
        images      : ['', [Validators.required]],
        category    : ['', [Validators.required]],
    });

    constructor(
        private fb: FormBuilder,
        private categoryService: CategoryService,
        private productService: ProductService,
        private router:Router,
    ) { }

    ngOnInit(): void {
        this.Form.patchValue({
            title: 'Titulo1',
            price: 1000,
            images: this.linkImg,
            description: 'Esto es una descripcion',
            category: this.Form.value.category.id,
        })

        this.categoryService.getAll()
            .subscribe({
                next: (values) => {
                    this.categories= values;
                    console.log(this.categories)
                }
            })
    };

    onSubmit(){
        if(!this.Form.valid) return;

        this.productDto = {
            images      : this.Form.value.images,
            title       : this.Form.value.title,
            price       : this.Form.value.price,
            description : this.Form.value.description,
            categoryId  : this.Form.value.category.id
        }
        console.log(this.productDto);
        this.productService.create(this.productDto)
            .subscribe({
                next: (value) => {
                    console.log(value)
                    this.Form.reset();
                    this.router.navigate(['dashboard/products']).then((r) => r)
                }
            })
    }

}
