import {Component, OnInit} from '@angular/core';
import {Product} from "../../../interfaces/product.interface";
import {ProductService} from "../../../services/product.service";

@Component({
    selector: 'app-products',
    templateUrl: './products.component.html',
    styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit{

    products: Array<Product> = [];

    constructor(
        private productService: ProductService
    ) { }

    ngOnInit(){

        this.productService.getAll(10,0)
            .subscribe({
                next: (values) => {
                    this.products = values;
                    console.log(values)
                },
                error: (err) => {
                    console.log(err)
                }
            })
    }
}
