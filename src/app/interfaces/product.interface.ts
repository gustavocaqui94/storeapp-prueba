//YApi QuickType插件生成，具体参考文档:https://github.com/RmondJone/YapiQuickType

export interface Product {
    images:      string[];
    price:       number;
    description: string;
    id:          number;
    title:       string;
    category:    Category;
}

export interface Category {
    name:    string;
    id:      number;
    typeImg: string;
}

export interface ProductDTO extends Omit<Product, 'id' | 'category'> {
    categoryId: number;
}