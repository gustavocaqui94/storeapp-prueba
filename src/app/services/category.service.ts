import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {Category} from "../interfaces/product.interface";
import {Observable} from "rxjs";

@Injectable({
    providedIn: 'root'
})
export class CategoryService {
    private _baseUrl: string = `${environment.API_URL}/categories`;

    constructor(
        private http: HttpClient,
    ) { }

    getAll(): Observable<Array<Category>>{
        return this.http.get<Array<Category>>(`${this._baseUrl}`);
    }
}
