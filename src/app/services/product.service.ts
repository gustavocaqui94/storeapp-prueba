import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {Product, ProductDTO} from "../interfaces/product.interface";
import {environment} from "../../environments/environment";

@Injectable({
    providedIn: 'root'
})
export class ProductService {
    private _baseUrl: string = `${environment.API_URL}/products`;

    constructor(
        private http: HttpClient
    ) { }

    getAll(limit?: number, offset?: number):Observable<Array<Product>>{
        // const url:string = `${this._baseUrl}`
        let params = new HttpParams();
        if (limit && offset) {
            params = params.set('limit', limit);
            params = params.set('offset', limit);
        }
        return this.http.get<Array<Product>>(`${ this._baseUrl }`,{params});
    }

    create(productDto: ProductDTO){
        return this.http.post<Product>(`${ this._baseUrl }`,productDto);
    }
}
