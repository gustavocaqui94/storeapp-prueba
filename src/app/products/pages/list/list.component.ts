import {Component, OnInit} from '@angular/core';
import {ProductService} from "../../../services/product.service";
import {Product} from "../../../interfaces/product.interface";
import {SelectItem} from "primeng/api";
import {Data, Event, EventType} from "@angular/router";

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit{
    products        !: Array<Product>;
    totalRecords    !: number;
    sortOptions     !: SelectItem[];
    sortKey         !: string;
    sortField       !: string;
    sortOrder       !: number;
    msg:string=``;

    constructor(
        private productService: ProductService,
    ) { }

    ngOnInit(){
        this.sortOptions = [
            {label: 'Price High to Low', value: '!price'},
            {label: 'Price Low to High', value: 'price'}
        ];
        this.productService.getAll()
            .subscribe({
                next: (values) => {
                    this.products = values;
                },
                error: err => {

                }
            })
    }

    loadData(event: Event) {
        //event.first = First row offset
        //event.rows = Number of rows per page
    }

    onSortChange(event: any): void{
        console.log(event);
        let value = event?.value ;

        if (value.indexOf('!') === 0) {
            this.sortOrder = -1;
            this.sortField = value.substring(1, value.length);
        }
        else {
            this.sortOrder = 1;
            this.sortField = value;
        }
    }

    onInput(event: any){
        let target: HTMLInputElement = event.target || '';
        this.msg = target.value.trim();
        this.products.filter( (item: Product)=> {
            if(this.msg === item.title){
                // this.products =
            }
        })


        console.log(this.msg );
        this.products = [];

    }
    //
    // change(event: Event){
    //     console.log(event)
    // }
    //
    onFocus(event: any){
        let val = event.value.filter() ;
        // console.log(data);
        console.log(val);
    }
    //
    // onDragover(event: Event){
    //     console.log(event)
    // }
}
