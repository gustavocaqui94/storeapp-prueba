import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductsRoutingModule } from './products-routing.module';
import { HomeComponent } from './pages/home/home.component';
import {SharedModule} from "../shared/shared.module";
import {PrimeNgModule} from "../prime-ng/prime-ng.module";
import { SearchComponent } from './pages/search/search.component';
import { ListComponent } from './pages/list/list.component';
import {FormsModule} from "@angular/forms";


@NgModule({
  declarations: [
    HomeComponent,
    SearchComponent,
    ListComponent
  ],
    imports: [
        CommonModule,
        ProductsRoutingModule,
        SharedModule,
        PrimeNgModule,
        FormsModule
    ]
})
export class ProductsModule { }
